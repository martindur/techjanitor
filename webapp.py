import os
from flask import Flask, render_template, send_file, abort, safe_join

BASE_DIR = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)
app.config["CLIENT_PDF"] = os.path.join(BASE_DIR, 'static', 'client', 'pdf')
app.config["STATIC_FOLDER"] = "/usr/src/app/techjanitor_org/static/"

@app.route('/')
def index():
    return render_template('index.html')


@app.route('/download_cv')
def download_cv():
    safe_path = safe_join(app.config["CLIENT_PDF"], "Martin_Durhuus_CV.pdf")
    return send_file(safe_path, as_attachment=True)


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)